#pragma once
#include <cmath>
class Vec3d
{
	double _x;
	double _y;
	double _z;
public:
	Vec3d();
	Vec3d(double x, double y, double z = 0);
	double getX() const { return _x; };
	double getY() const { return _y; };
	double getZ() const { return _z; };
	double magnitude() const { return sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2)); };
	void setX(double newXVal){ _x = newXVal; };
	void setY(double newYVal){ _y = newYVal; };
	void setZ(double newZVal){ _z = newZVal; };
	void setXYZ(double newXVal, double newYVal, double newZVal);
	Vec3d &operator=(const Vec3d &other);
	Vec3d &operator!=(const Vec3d &other);
	double crossDir(Vec3d &other); //returns z value of two dimensional cross product
	~Vec3d();
	void print();
};

