#pragma once
#include <vector>
#include "Vec3d.h"
#include "Grid.h"
enum class SliceDir
{
	x_front, x_back, y_left, y_right, Diagonal_bl, Diagonal_br, Diagonal_tl, Diagonal_tr //bottom left, bottom right, top left, top right
};
class Contours
{
	std::vector<std::vector<Vec3d> > _contours; 
	SliceDir _dir;
public:
	Contours();
	Contours(Grid &grid, SliceDir dir);
	void drawContour(int idx);
	std::vector<Vec3d> getContour(Grid &grid, SliceDir dir, int sliceNum);
	std::vector<Vec3d> compareContours(std::vector<Vec3d> con1, std::vector<Vec3d> con2);
	std::vector<Vec3d> findHorizon();
	bool intersectionTest(Vec3d vec1, Vec3d vec2, Vec3d vec3, Vec3d vec4); //returns true if the they intersect, false if they don't
	Vec3d findSegmentIntersection(Vec3d vec1, Vec3d vec2, Vec3d vec3, Vec3d vec4); //returns an (x,y,0) vector
	bool ifBelow(Vec3d vec1, Vec3d vec2, Vec3d vec3, Vec3d vec4); //returns true if segment(vec1, vec2) is below segment(vec3, vec4), false otherwise;
	double yVal(double x, int conIdx); //returns the interpolated y value of a given x value on a contour at index conIdx 
	std::vector<Vec3d> findHillPoints(double step);
	std::vector<std::vector<Vec3d> > findConnectedHillPoints(double step);
	double maxVal(double val1, double val2);
	double minVal(double val1, double val2);
	bool onSegment(Vec3d vec1, Vec3d vec2, Vec3d vec3);
	int orientation(Vec3d vec1, Vec3d vec2, Vec3d vec3);
	void drawXOrder(int idx1, int idx2);
	std::vector<std::vector<int> >findAndInsertIntersections(std::vector<Vec3d> &con1, std::vector<Vec3d> &con2); //finds the intersections of two contours, then inserts the intersection points into each contour, ordered by x
																									   //also returns a list of the indexs of each intersection, 0 is con1, 1 is con2;
	~Contours();
};

