#include "Segment.h"
#include "Vec3d.h"
#include <GL/freeglut.h>



Segment::Segment()
{
}

Segment::Segment(Vec3d v1, Vec3d v2)
{
	_v1 = v1;
	_v2 = v2;
}

Segment::~Segment()
{
}

void Segment::print()
{
	_v1.print();
	_v2.print();
}

void Segment::draw()
{
	glBegin(GL_LINES);
	{
		glVertex2d(_v1.getX(), _v1.getY());
		glVertex2d(_v2.getX(), _v2.getY());
	}
	glEnd();
}

void Segment::v1setXYZ(double newXVal, double newYVal, double newZVal)
{
	_v1.setX(newXVal);
	_v1.setY(newYVal);
	_v1.setZ(newZVal);
}
void Segment::v2setXYZ(double newXVal, double newYVal, double newZVal)
{
	_v2.setX(newXVal);
	_v2.setY(newYVal);
	_v2.setZ(newZVal);
}