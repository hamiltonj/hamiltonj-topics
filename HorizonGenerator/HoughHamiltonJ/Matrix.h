#pragma once
#include <vector>
class Vec3d;
class Segment;

class Matrix
{
	std::vector<double> _values;
	double _squareLength;
public:
	Matrix(std::vector<double> values);
	double getVal(int x, int y);
	Matrix();
	~Matrix();
	Vec3d multiply(Vec3d vec);
	Matrix multiply(Matrix mat);
	Segment multiply(Segment seg);
	void print();
};

