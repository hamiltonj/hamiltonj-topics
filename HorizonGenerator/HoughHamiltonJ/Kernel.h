#pragma once
#include <vector>
#include "ImageDraw.h"
class Pic;

class Kernel
{
public:
	std::vector<double> _matrix;
	int _width;
	int _height;
	double _offset;
	bool _normalize;

	Kernel();
	Kernel(int width, int height, std::vector<double> matrix, bool normalize, double offset = 0);
	~Kernel();
	RGBPixel apply(Pic &pic, int x, int y);
}; 
