#include "Pic.h"
#include "Soil\SOIL.h"
#include <cmath>

Kernel createSobelX();
Kernel createSobelY();
/*
std::string _filename;
int _width;
int _height;
RGBPixel *_pixels;
*/

Pic::Pic(const char *filename)
{
	_filename = filename;
	_pixels = loadImage(filename, &_width, &_height);
	_textureId = createTexture();
}
	
Pic::Pic(int width, int height)
{
	_width = width;
	_height = height;
	_pixels = new RGBPixel[_width*_height];
	_textureId = createTexture();
}

Pic::Pic(const Pic &other)
{
	_width = other.width();
	_height = other.height();
	_pixels = new RGBPixel[_width*_height];
	_textureId = createTexture();

	for (int i = 0; i < (_width*_height); i++)
	{
		_pixels[i] = other._pixels[i];
	}
}

Pic::Pic(Pic &other, bool toGrey)
{
	_width = other.width();
	_height = other.height();
	_pixels = new RGBPixel[_width*_height];
	_textureId = createTexture();
	
	
	for (int x = 0; x < other.width(); x++)
	{
		for (int y = 0; y < other.height(); y++)
		{
			if (toGrey)
			{
				setGrey(x, y, other.getGrey(x, y));
			}
			else
			{
				pixel(x, y) = other.pixel(x, y);
			}
		}
	}
}

Pic::~Pic()
{
	delete[] ((unsigned char *)_pixels);
}

Pic &Pic::operator=(const Pic &other)
{
	delete[]((unsigned char *)_pixels);

	_width = other.width();
	_height = other.height();
	_pixels = new RGBPixel[_width*_height];
	_textureId = createTexture();

	for (int i = 0; i < (_width*_height); i++)
	{
		_pixels[i] = other._pixels[i];
	}

	return *this;
}

void Pic::save(const char *filename)
{
	SOIL_save_image(filename, SOIL_SAVE_TYPE_BMP, _width, _height, SOIL_LOAD_RGB, (unsigned char *)_pixels);
}

void Pic::draw(double x, double y, double width, double height)
{
	drawTexture(_textureId, x, y, width, height, _width, _height, _pixels);
}

RGBPixel &Pic::pixel(int x, int y)
{
	return _pixels[y*_width + x];
}

int Pic::getGrey(int x, int y)
{
	int r = pixel(x, y).r; //.21
	int b = pixel(x, y).b; //.07
	int g = pixel(x, y).g; //.72

	return (.21 * r) + (.72 * g) + (.07 * b);
}

void Pic::setGrey(int x, int y, int value)
{
	pixel(x, y).r = value;
	pixel(x, y).b = value;
	pixel(x, y).g = value;
}

void Pic::threshold(int level)
{
	for (int x = 0; x < _width; x++)
	{
		for (int y = 0; y < _height; y++)
		{
			if (pixel(x, y).r > level && pixel(x, y).g > level && pixel(x, y).b > level)
			{
				pixel(x, y).r = 255;
				pixel(x, y).g = 255;
				pixel(x, y).b = 255;
			}
			else
			{
				pixel(x, y).r = 0;
				pixel(x, y).g = 0;
				pixel(x, y).b = 0;
			}
		}
	}
}

void Pic::derivativeX()
{
	for (int x = 0; x < _width - 1; x++)
	{
		for (int y = 0; y < _height; y++)
		{
			double value = abs(pixel(x, y).r - pixel(x + 1, y).r);
			pixel(x, y).r = value;
			pixel(x, y).b = value;
			pixel(x, y).g = value;
		}
	}
}
void Pic::derivativeY()
{
	for (int x = 0; x < _width; x++)
	{
		for (int y = 0; y < _height - 1; y++)
		{
			double value = abs(pixel(x, y).r - pixel(x, y+1).r);
			pixel(x, y).r = value;
			pixel(x, y).b = value;
			pixel(x, y).g = value;
		}
	}
}

void Pic::derivativeXandY()
{
	for (int x = 0; x < _width; x++)
	{
		for (int y = 0; y < _height - 1; y++)
		{
			double value1 = abs(pixel(x, y).r - pixel(x+1, y).r);
			double value2 = abs(pixel(x, y).r - pixel(x + 1, y).r);
			double value3 = sqrt(pow(value1, 2) + pow(value2, 2));
			pixel(x, y).r = value3;
			pixel(x, y).b = value3;
			pixel(x, y).g = value3;
		}
	}
}

Pic Pic::apply(Kernel k)
{
	int xOffset = k._width / 2;
	int yOffset = k._height / 2;
	Pic temp(_width, _height);
	
	for (int row = xOffset; row < _width - xOffset; row++)
	{
		for (int col = yOffset; col < _height - yOffset; col++)
		{
			temp.pixel(row, col) = k.apply(*this, row, col);
		}
	}

	return temp;
}
/*
Pic Pic::applySobelGradient()
{
	Kernel kX = createSobelX();
	Kernel kY = createSobelY();
	Pic tempX(*this);
	Pic tempY(*this);
	Pic tempG(_width, _height);

	tempX.apply(kX);
	tempY.apply(kY);

	for (int row = 0; row < tempX.width(); row++)
	{
		for (int col = 0; col < tempX.height(); col++)
		{
			tempG.pixel(row, col).r = sqrt(pow(tempX.pixel(row, col).r, 2) + pow(tempY.pixel(row, col).r, 2));
			tempG.pixel(row, col).b = sqrt(pow(tempX.pixel(row, col).b, 2) + pow(tempY.pixel(row, col).b, 2));
			tempG.pixel(row, col).g = sqrt(pow(tempX.pixel(row, col).g, 2) + pow(tempY.pixel(row, col).g, 2));
		}
	}

	return tempG;
}*/

std::vector<std::vector<int>>createMatrix(int width, int height)
{
	std::vector<std::vector<int>> matrix;

	for (int x = 0; x < width; x++)
	{
		matrix.push_back(std::vector<int>(height));
	}

	return matrix;
}

Pic Pic::houghTransform()
{
	std::vector<std::vector<int>> votes = createMatrix(_width, _height);

	for (int row = 0; row < votes.size(); row++)
	{
		for (int col = 0; col < votes[0].size(); col++)
		{
			//if r>0 and |r| < sqrt(width^2 + height^2)
			if (pixel(row, col).r > 128)
			{
				for (int i = 0; i < 500; i++)
				{
					double theta = 2 * 3.1415926535897 / 500 * i;
					double r = abs(row*cos(theta) + col*sin(theta));
					r /= sqrt(pow(width(), 2) + pow(height(), 2));
					r *= _height;
					if (r < _height)
					{
						votes[i][r]++;
					}
				}
			}
		}
	}

	int maximumVotes = 0;

	for (int i = 0; i < votes.size(); i++)
	{
		for (int j = 0; j < votes[i].size(); j++)
		{
			if (votes[i][j] > maximumVotes)
			{
				maximumVotes = votes[i][j];
			}
		}
	}

	double scalar = 255.0 / maximumVotes;

	Pic temp = Pic(_width, _height);
	for (int x = 0; x < temp.width(); x++)
	{
		for (int y = 0; y < temp.height(); y++)
		{
			temp.pixel(x, y).r = votes[x][y] * scalar;
			temp.pixel(x, y).b = votes[x][y] * scalar;
			temp.pixel(x, y).g = votes[x][y] * scalar;
		}
	}

	return temp;
}