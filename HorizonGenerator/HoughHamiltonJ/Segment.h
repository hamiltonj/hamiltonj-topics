#pragma once
#include "Vec3d.h"
class Segment
{
	Vec3d _v1;
	Vec3d _v2;
public:
	Segment();
	Segment(Vec3d v1, Vec3d v2);
	~Segment();
	double v1X() const { return _v1.getX(); };
	double v1Y() const { return _v1.getY(); };
	double v1Z() const { return _v1.getZ(); };
	double v2X() const { return _v2.getX(); };
	double v2Y() const { return _v2.getY(); };
	double v2Z() const { return _v2.getZ(); };
	void v1setX(double newXVal){ _v1.setX(newXVal); };
	void v1setY(double newYVal){ _v1.setY(newYVal); };
	void v1setZ(double newZVal){ _v1.setZ(newZVal); };
	void v2setX(double newXVal){ _v2.setX(newXVal); };
	void v2setY(double newYVal){ _v2.setX(newYVal); };
	void v2setZ(double newZVal){ _v2.setX(newZVal); };
	void v1setXYZ(double newXVal, double newYVal, double newZVal);
	void v2setXYZ(double newXVal, double newYVal, double newZVal);
	void print();
	void draw();

};

