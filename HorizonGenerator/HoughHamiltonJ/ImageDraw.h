#pragma once

#include "GL\freeglut.h"

struct RGBPixel
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

GLuint createTexture();
void drawTexture(GLuint texture_id, double x, double y, double w, double h, int imageWidth, int imageHeight, RGBPixel *rawPixels);
RGBPixel *loadImage(const char *name, int *width, int *height);