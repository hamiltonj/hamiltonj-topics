#include "ImageDraw.h"
#include "GL\freeglut.h"
#include "Soil\SOIL.h"

GLuint createTexture()
{
	GLuint texture_id;
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	return texture_id;
}

void drawTexture(GLuint texture_id, double x, double y, double w, double h, int imageWidth, int imageHeight, RGBPixel *rawPixels)
{
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, rawPixels);
	glEnable(GL_TEXTURE_2D);
	glColor3ub(255, 255, 255);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 1.0);
	glVertex3d(x, y, 0.0);
	glTexCoord2f(1.0, 1.0);
	glVertex3d(x+w, y, 0.0);
	glTexCoord2f(1.0, 0.0);
	glVertex3d(x+w, y+h, 0.0);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(x, y+h, 0.0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

RGBPixel *loadImage(const char *name, int *width, int *height)
{
	int channels;

	return (RGBPixel*)SOIL_load_image(name, width, height, &channels, SOIL_LOAD_RGB);
}