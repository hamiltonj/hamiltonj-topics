#pragma once
#include <vector>
#include "Vec3d.h"
class Matrix;

class Grid
{
	std::vector<Vec3d> _points;
	int _width;
	int _height;
public:
	Grid();
	Grid(int width, int height);
	Grid(int width, int height, std::vector<Vec3d> points);
	std::vector<Vec3d> getPoints() { return _points; };
	Vec3d &at(int x, int y) { return _points[x + y*_height]; };
	int width() { return _width; };
	int height() { return _height; };
	void applyMatrixInPlace(Matrix mat);
	Grid applyMatrix(Matrix mat);
	void drawWithClipping(double n); //n is the near clipping plane
	~Grid();

	//make a method get contour that returns a vector and takes arguments that would be a slice number and orientation
	//num slices method that takes an orientation
};

