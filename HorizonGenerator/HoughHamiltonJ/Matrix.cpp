#include "Matrix.h"
#include <vector>
#include <iostream>
#include "Vec3d.h"
#include "Segment.h"

using namespace std;

Matrix::Matrix()
{
}

Matrix::Matrix(std::vector<double> values)
{
	_values = values;
	_squareLength = sqrt(values.size());
}

Matrix::~Matrix()
{

}

double Matrix::getVal(int x, int y)
{
	return _values[x + _squareLength * y];
}

void Matrix::print()
{
	for (int i = 0; i < _squareLength; i++)
	{
		for (int j = 0; j < _squareLength; j++)
		{
			cout << getVal(j, i) << "  ";
		}
		cout << endl;
	}
}

Vec3d Matrix::multiply(Vec3d vec)
{
	Vec3d newVec;
	if (_squareLength == 4)
	{
		double newX = getVal(0, 0)*vec.getX() + getVal(1, 0)*vec.getY() + getVal(2, 0)*vec.getZ() + getVal(3, 0);
		double newY = getVal(0, 1)*vec.getX() + getVal(1, 1)*vec.getY() + getVal(2, 1)*vec.getZ() + getVal(3, 1);
		double newZ = getVal(0, 2)*vec.getX() + getVal(1, 2)*vec.getY() + getVal(2, 2)*vec.getZ() + getVal(3, 2);
		double newW = getVal(0, 3)*vec.getX() + getVal(1, 3)*vec.getY() + getVal(2, 3)*vec.getZ() + getVal(3, 3);

		newVec.setXYZ(newX/newW, newY/newW, newZ/newW);
	}
	return newVec;
}

Segment Matrix::multiply(Segment seg)
{
	Segment newSeg;
	if (_squareLength == 4)
	{
		double newX1 = getVal(0, 0)*seg.v1X() + getVal(1, 0)*seg.v1Y() + getVal(2, 0)*seg.v1Z() + getVal(3, 0);
		double newY1 = getVal(0, 1)*seg.v1X() + getVal(1, 1)*seg.v1Y() + getVal(2, 1)*seg.v1Z() + getVal(3, 1);
		double newZ1 = getVal(0, 2)*seg.v1X() + getVal(1, 2)*seg.v1Y() + getVal(2, 2)*seg.v1Z() + getVal(3, 2);
		double newW1 = getVal(0, 3)*seg.v1X() + getVal(1, 3)*seg.v1Y() + getVal(2, 3)*seg.v1Z() + getVal(3, 3);

		newSeg.v1setXYZ(newX1 / newW1, newY1 / newW1, newZ1 / newW1);

		double newX2 = getVal(0, 0)*seg.v2X() + getVal(1, 0)*seg.v2Y() + getVal(2, 0)*seg.v2Z() + getVal(3, 0);
		double newY2 = getVal(0, 1)*seg.v2X() + getVal(1, 1)*seg.v2Y() + getVal(2, 1)*seg.v2Z() + getVal(3, 1);
		double newZ2 = getVal(0, 2)*seg.v2X() + getVal(1, 2)*seg.v2Y() + getVal(2, 2)*seg.v2Z() + getVal(3, 2);
		double newW2 = getVal(0, 3)*seg.v2X() + getVal(1, 3)*seg.v2Y() + getVal(2, 3)*seg.v2Z() + getVal(3, 3);

		newSeg.v2setXYZ(newX2 / newW2, newY2 / newW2, newZ2 / newW2);
	}
	return newSeg;
}

double calculateTerm(Matrix mat1, Matrix mat2, int x, int y)//calculates a specific xy value in the multiplication of two 4x4 matrices
{
	double val = 0;
	for (int i = 0; i < 4; i++)
	{
		val += mat1.getVal(i, y)*mat2.getVal(x, i);
	}
	return val;
		
}

Matrix Matrix::multiply(Matrix mat)
{
	vector<double> newMatrix;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			newMatrix.push_back(calculateTerm(*this, mat, j, i));
		}
	}

	return Matrix(newMatrix);
}
