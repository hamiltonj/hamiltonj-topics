/* fractals.c */

#include <iostream>
#include <GL/freeglut.h>
#include <cstdlib>
#include <string>
#include <cmath>
#include <vector>
#include "Vertex.h"
#include <ctime>



#define FGH_PI       3.14159265358979323846

/* Flag telling us to keep executing the main loop */
static int continue_in_main_loop = 1;

/* the window title */
char window_title[80];

/* The amount the view is translated and scaled */
double xwin = 0.0, ywin = 0.0;
double scale_factor = 1.0;

using namespace std;

vector<Vertex> points;

static void
Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	/* the curve */
	glPushMatrix();
	//glScalef(2.5, 2.5, 2.5);

	glColor4f(0.0, 0.0, 0.0, 1.0);
	glBegin(GL_POINTS);
	
	for (int i = 0; i < points.size(); i++)
	{
		glVertex2d(points[i].x, points[i].y);
	}
	//glVertex2d(0, 0);
	//glVertex2d(1, 0);

	glEnd();

	glPopMatrix();
	glutSwapBuffers();
}

static void
Reshape(int width, int height)
{
	float ar;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1.0, 1.0, -1, 1, 2.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	xwin = 0.0;
	ywin = 0.0;
	glTranslated(xwin, ywin, -5.0);
}

static void
Key(unsigned char key, int x, int y)
{
	int need_redisplay = 1;

	switch (key) {
	case 27:  /* Escape key */
		continue_in_main_loop = 0;
		break;

	case '+':

		break;

	case '-':

		break;

	case 'r':  case 'R':
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		xwin = -1.0;
		ywin = 0.0;
		glTranslated(xwin, ywin, -5.0);
		break;

	default:
		need_redisplay = 0;
		break;
	}
	if (need_redisplay)
		glutPostRedisplay();
}

static void
Special(int key, int x, int y)
{
	int need_redisplay = 1;

	switch (key) {
	case GLUT_KEY_UP:
		glMatrixMode(GL_MODELVIEW);
		ywin += 0.1 * scale_factor;
		glTranslated(0.0, 0.1 * scale_factor, 0.0);
		break;

	case GLUT_KEY_DOWN:
		glMatrixMode(GL_MODELVIEW);
		ywin -= 0.1 * scale_factor;
		glTranslated(0.0, -0.1 * scale_factor, 0.0);
		break;

	case GLUT_KEY_LEFT:
		glMatrixMode(GL_MODELVIEW);
		xwin -= 0.1 * scale_factor;
		glTranslated(-0.1 * scale_factor, 0.0, 0.0);
		break;

	case GLUT_KEY_RIGHT:
		glMatrixMode(GL_MODELVIEW);
		xwin += 0.1 * scale_factor;
		glTranslated(0.1 * scale_factor, 0.0, 0.0);
		break;

	case GLUT_KEY_PAGE_UP:
		glMatrixMode(GL_MODELVIEW);
		glTranslated(-xwin, -ywin, 0.0);
		glScaled(1.25, 1.25, 1.25);
		glTranslated(xwin, ywin, 0.0);
		scale_factor *= 0.8;
		break;

	case GLUT_KEY_PAGE_DOWN:
		glMatrixMode(GL_MODELVIEW);
		glTranslated(-xwin, -ywin, 0.0);
		glScaled(0.8, 0.8, 0.8);
		glTranslated(xwin, ywin, 0.0);
		scale_factor *= 1.25;
		break;

	default:
		need_redisplay = 0;
		break;
	}
	if (need_redisplay)
		glutPostRedisplay();
}


void MouseFunc(int button, int state, int x, int y)
{
	///std::cout << "Got a mouse event: " << button << " " << state << " " << x << " " << y << "\n";
}

void MouseMotion(int x, int y)
{
	//std::cout << "Motion: " << x << " " << y << std::endl;
}

void MousePassiveMotion(int x, int y)
{
	//std::cout << "Passive Motion: " << x << " " << y << std::endl;
}

int main(int argc, char *argv[])
{
	srand(time(NULL));


	for (int i = 0; i < 100; i++)
	{
		Vertex point;
		point.x = (rand() % 10000 - 5000)/10000;
		point.y = (rand() % 10000 - 5000) / 10000;

		points.push_back(point);
	}

	int fractal_window;

	glutInitWindowSize(500, 250);
	glutInitWindowPosition(140, 140);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInit(&argc, argv);
	fractal_window = glutCreateWindow(window_title);

	glClearColor(1.0, 1.0, 1.0, 1.0);

	glutMouseFunc(MouseFunc);
	glutMotionFunc(MouseMotion);
	glutPassiveMotionFunc(MousePassiveMotion);

	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Key);
	glutSpecialFunc(Special);
	glutDisplayFunc(Display);

#ifdef WIN32
#endif

	while (continue_in_main_loop)
		glutMainLoopEvent();

	printf("Back from the 'freeglut' main loop\n");

	return 0;             /* ANSI C requires main to return int. */
}