#include "Kernel.h"
#include <vector>
#include "Pic.h"
#include <iostream>

Kernel::Kernel()
{
}

Kernel::Kernel(int width, int height, std::vector<double> matrix, bool normalize, double offset)
{
	_width = width;
	_height = height;
	_matrix = matrix; 
	_offset = offset;
	_normalize = normalize;
}

Kernel::~Kernel()
{
}

RGBPixel Kernel::apply(Pic &pic, int x, int y)
{
	int xOffset = _width / 2;
	int yOffset = _height / 2;
	double totalr = 0;
	double totalb = 0;
	double totalg = 0;

	for (int i = 0; i < _width; i++)
	{
		for (int j = 0; j < _height; j++)
		{
			totalr += pic.pixel(x - xOffset + i, y - yOffset + j).r * _matrix[j + _width * i];
			totalb += pic.pixel(x - xOffset + i, y - yOffset + j).b * _matrix[j + _width * i];
			totalg += pic.pixel(x - xOffset + i, y - yOffset + j).g * _matrix[j + _width * i];
		}
	}

	if (_normalize)
	{
		totalr /= _matrix.size();
		totalb /= _matrix.size();
		totalg /= _matrix.size();
	}

	totalr += _offset;
	totalg += _offset;
	totalb += _offset;

	RGBPixel pixel = RGBPixel();
	pixel.r = totalr;
	pixel.b = totalb;
	pixel.g = totalg;

	return pixel;
}

