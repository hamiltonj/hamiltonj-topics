#include "Contours.h"
#include <vector>
#include <iostream>
#include <GL/freeglut.h>

using namespace std;

Contours::Contours()
{
}

Contours::~Contours()
{
}

vector<Vec3d> Contours::getContour(Grid &grid, SliceDir dir, int sliceNum)
{
	vector<Vec3d> contour;

	switch (dir)
	{
	case SliceDir::x_front:
		for (int i = 0; i < grid.width(); i++)
		{
			contour.push_back(grid.at(i, sliceNum));
		}
		break;
	case SliceDir::x_back:
		for (int i = 0; i < grid.width(); i++)
		{
			contour.push_back(grid.at(grid.width() - i - 1, grid.height() - sliceNum - 1));
		}
		break;
	case SliceDir::y_left:
		for (int i = grid.height() - 1; i >= 0; i--)
		{
			contour.push_back(grid.at(sliceNum, i));
		}
		break;
	case SliceDir::y_right:
		for (int i = grid.height() - 1; i >= 0; i--)
		{
			contour.push_back(grid.at(grid.width() - sliceNum - 1, grid.height() - i - 1));
		}
		break;
	}

	return contour;
}

Contours::Contours(Grid &grid, SliceDir dir)
{
	for (int i = 0; i < grid.height(); i++)
	{
		_contours.push_back(getContour(grid, dir, i));
	}
	_dir = dir;
}

void Contours::drawContour(int idx)
{
	glColor4f(0.0, 0.0, 0.0, 1.0);
	glBegin(GL_POINTS);
	{
		for (int i = 0; i < _contours[idx].size(); i++)
		{
			glVertex2d(_contours[idx][i].getX(), _contours[idx][i].getY());
		}
	}
	glEnd();
}

double Contours::maxVal(double val1, double val2)
{
	if (val1 > val2)
	{
		return val1;
	}
	else
	{
		return val2;
	}
}

double Contours::minVal(double val1, double val2)
{
	if (val1 < val2)
	{
		return val1;
	}
	else
	{
		return val2;
	}
}


bool Contours::onSegment(Vec3d vec1, Vec3d vec2, Vec3d vec3) // given three colinear points, the function checks if vec2 is on segment vec1_vec3
{
	if (vec2.getX() <= maxVal(vec1.getX(), vec3.getX()) && vec2.getX() >= minVal(vec1.getX(), vec3.getX()) &&
		vec2.getY() <= maxVal(vec1.getY(), vec3.getY()) && vec2.getY() >= minVal(vec1.getY(), vec3.getY()))
	{
		return true;
	}
	else
	{
		return false;
	}
}

int Contours::orientation(Vec3d vec1, Vec3d vec2, Vec3d vec3)//returns 0 if colinear, 1 if clockwise, 2 if counterclockwise
{
	double val = (vec2.getY() - vec1.getY())*(vec3.getX() - vec2.getX()) -
		         (vec2.getX() - vec1.getX())*(vec3.getY() - vec2.getY());

	if (val == 0) return 0;  // colinear

	if (val > 0) //clockwise
	{
		return 1;
	}
	else //counterclockwise
	{
		return 2;
	}
}

bool Contours::intersectionTest(Vec3d vec1, Vec3d vec2, Vec3d vec3, Vec3d vec4)
{
	// Find the four orientations needed for general and
	// special cases
	int o1 = orientation(vec1, vec2, vec3);
	int o2 = orientation(vec1, vec2, vec4);
	int o3 = orientation(vec3, vec4, vec1);
	int o4 = orientation(vec3, vec4, vec2);

	// General case
	if (o1 != o2 && o3 != o4)
		return true;

	// Special Cases
	if (o1 == 0 && onSegment(vec1, vec3, vec2)) return true;
	if (o2 == 0 && onSegment(vec1, vec4, vec2)) return true;
	if (o3 == 0 && onSegment(vec3, vec1, vec4)) return true;
	if (o4 == 0 && onSegment(vec3, vec2, vec4)) return true;

	return false; // Doesn't fall in any of the above cases

}

Vec3d Contours::findSegmentIntersection(Vec3d vec1, Vec3d vec2, Vec3d vec3, Vec3d vec4)
{
	Vec3d interVec;

	double m1 = (vec2.getY() - vec1.getY()) / (vec2.getX() - vec1.getX());
	double m2 = (vec4.getY() - vec3.getY()) / (vec4.getX() - vec3.getX());

	double b1 = vec1.getY() - (m1*vec1.getX());
	double b2 = vec3.getY() - (m2*vec3.getX());

	double x = (b2 - b1) / (m1 - m2);
	double y = m1*x + b1;

	interVec.setX(x);
	interVec.setY(y);
	interVec.setZ(0);

	return interVec;

}

bool Contours::ifBelow(Vec3d vec1, Vec3d vec2, Vec3d vec3, Vec3d vec4)
{
	if (vec1.getX() > vec3.getX() && vec2.getX() < vec4.getX())
	{
		double m34 = (vec4.getY() - vec3.getY()) / (vec4.getX() - vec3.getX());
		double y = m34*vec2.getX() - m34*(vec3.getX()) + vec3.getY();

		if (vec2.getY() < y)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	if (vec3.getX() > vec1.getX() && vec4.getX() < vec2.getX())
	{
		double m12 = (vec2.getY() - vec1.getY()) / (vec2.getX() - vec1.getX());
		double y = m12*vec4.getX() - m12*vec1.getX() + vec1.getY();

		if (vec4.getY() < y)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	if (vec1.getX() < vec3.getX())
	{
		double m34 = (vec4.getY() - vec3.getY()) / (vec4.getX() - vec3.getX());
		double y = m34*vec2.getX() - m34*(vec3.getX()) + vec3.getY();

		if (vec2.getY() < y)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	if (vec3.getX() < vec1.getX())
	{
		double m12 = (vec2.getY() - vec1.getY()) / (vec2.getX() - vec1.getX());
		double y = m12*vec4.getX() - m12*vec1.getX() + vec1.getY();

		if (vec4.getY() < y)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	if (vec1.getX() == vec3.getX())
	{
		if (vec1.getY() < vec3.getY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	if (vec2.getX() == vec4.getX())
	{
		if (vec2.getY() < vec4.getY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}

vector<vector<int> > Contours::findAndInsertIntersections(std::vector<Vec3d> &con1, std::vector<Vec3d> &con2)
{
	vector<vector<int> > intersectionIndexes(2);
	for (int idx1 = 0; idx1 < con1.size() - 1; idx1++)
	{
		for (int idx2 = 0; idx2 < con2.size() - 1; idx2++)
		{
			if (intersectionTest(con1[idx1], con1[idx1 + 1], con2[idx2], con2[idx2 + 1])) //found an intersection
			{
				Vec3d intersectionPoint = findSegmentIntersection(con1[idx1], con1[idx1 + 1], con2[idx2], con2[idx2 + 1]);  //determine the coordinates of the intersection
				
				if (intersectionPoint.getX() == con1[idx1].getX() && intersectionPoint.getY() == con1[idx1].getY())
				{
					intersectionIndexes[0].push_back(idx1);
				}
				else
				{
					intersectionIndexes[0].push_back(idx1 + 1);
					con1.insert(con1.begin() + idx1 + 1, intersectionPoint);
				}

				if (intersectionPoint.getX() == con2[idx2].getX() && intersectionPoint.getY() == con2[idx2].getY())
				{
					intersectionIndexes[1].push_back(idx2);
				}
				else
				{
					intersectionIndexes[1].push_back(idx2 + 1);
					con2.insert(con2.begin() + idx2 + 1, intersectionPoint);
				}
				idx1++;
				idx2++;
			}
		}
	}
	return intersectionIndexes;
}

std::vector<Vec3d> Contours::compareContours(std::vector<Vec3d> con1, std::vector<Vec3d> con2)
{
	vector<Vec3d> newContour;
	vector<vector<int>> intersectionIndexes = findAndInsertIntersections(con1, con2);
	bool con1isAbove;
	bool intersectionsLeft = true;
	int intersectIdx1 = 0;
	int intersectIdx2 = 0;
	int idx1 = 0;
	int idx2 = 0;
	
	if (intersectionIndexes[0].size() == 0)
	{
		while (con1[idx1 + 1].getX() < con2[idx2].getX())
		{
			idx1++;
		}
		while (con2[idx2 + 1].getX() < con1[idx1].getX())
		{
			idx2++;
		}

		if (ifBelow(con1[idx1], con1[idx1 + 1], con2[idx2], con2[idx2 + 1]))
		{
			return con2;
		}
		else
		{
			return con1;
		}
	}

	while (con1[idx1 + 1].getX() < con2[idx2].getX())
	{
		//newContour.push_back(con1[idx1]);
		idx1++;
	}
	while (con2[idx2 + 1].getX() < con1[idx1].getX())
	{
		//newContour.push_back(con2[idx2]);
		idx2++;
	}

	if (intersectionTest(con1[idx1], con1[idx1 + 1], con2[idx2], con2[idx2 + 1]))
	{
		if (con1[idx1].getY() > con2[idx2].getY())
		{
			con1isAbove = true;
		}
		else
		{
			con1isAbove = false;
		}
	}
	else
	{ 
		if (ifBelow(con1[idx1], con1[idx1 + 1], con2[idx2], con2[idx2 + 1]))
		{
			con1isAbove = false;
		}
		else
		{
			con1isAbove = true;
		}
	}
	idx1 = 0;
	idx2 = 0;//after the initial test of which one is above, put the indexes back to zero

	while (intersectionsLeft)
	{
		if (con1isAbove)
		{
			while (idx1 < intersectionIndexes[0][intersectIdx1])
			{
				newContour.push_back(con1[idx1]);
				idx1++;
			}
			while (idx2 < intersectionIndexes[1][intersectIdx2])
			{
				idx2++;
			}
			newContour.push_back(con1[idx1]);
			idx1++;
			idx2++;
		}
		if (!con1isAbove)
		{
			while (idx1 < intersectionIndexes[0][intersectIdx1])
			{
				idx1++;
			}
			while (idx2 < intersectionIndexes[1][intersectIdx2])
			{
				newContour.push_back(con2[idx2]);
				idx2++;
			}
			newContour.push_back(con2[idx2]);
			idx1++;
			idx2++;
		}
		
		if (intersectIdx1 == intersectionIndexes[0].size() - 1)
		{
			intersectionsLeft = false;
		}
		else
		{
			intersectIdx1++;
			intersectIdx2++;


			if (idx1 == intersectionIndexes[0][intersectIdx1] || idx2 == intersectionIndexes[1][intersectIdx2])
			{
				con1isAbove = !con1isAbove;
			}
			else if (ifBelow(con1[idx1], con1[idx1 + 1], con2[idx2], con2[idx2 + 1]))
			{
				con1isAbove = false;
			}
			else
			{
				con1isAbove = true;
			}
		}
		/*else if (con1[idx1].getY() > con2[idx2].getY())
		{
			con1isAbove = true;
		}
		else if (con1[idx1].getY() == con2[idx2].getY())
		{
			if (con1[idx1].getX() < con2[idx2].getX())
			{
				con1isAbove = true;
			}
			if (con1[idx1].getX() > con2[idx2].getX())
			{
				con1isAbove = false;
			}
		}
		else
		{
			con1isAbove = false;
		}
		*/
		//con1isAbove = !con1isAbove;
	}

	if (idx1 + 2 < con1.size() && idx2 + 2 < con2.size())
	{
		if (ifBelow(con1[idx1], con1[idx1 + 1], con2[idx2], con2[idx2 + 1]))
		{
			con1isAbove = false;
		}
		else
		{
			con1isAbove = true;
		}
	}
	else
	{
		if (con1[idx1].getY() > con2[idx2].getY())
		{
			con1isAbove = true;
		}
		else
		{
			con1isAbove = false;
		}
	}

	if (con1isAbove)
	{
		while (idx1 < con1.size())
		{
			newContour.push_back(con1[idx1]);
			idx1++;
		}
	}
	if (!con1isAbove)
	{
		while (idx2 < con2.size())
		{
			newContour.push_back(con2[idx2]);
			idx2++;
		}
	}

	while (idx1 < con1.size())
	{
		if (con1[idx1].getX() > con2[con2.size() - 1].getX())
		{
			newContour.push_back(con1[idx1]);
		}
		idx1++;
	}
	while (idx2 < con2.size())
	{
		if (con2[idx2].getX() > con1[con1.size() - 1].getX())
		{
			newContour.push_back(con2[idx2]);
		}
		idx2++;
	}
	

	return newContour;

	/*vector<Vec3d> newContour;
	vector<Vec3d> intersectionPoints = findAndInsertIntersections(con1, con2);
	bool con1isAbove;
	int intersectIdx = 0;
	int idx1 = 0;
	int idx2 = 0;

	if (intersectionPoints.size() == 0)
	{
		while (con1[idx1 + 1].getX() < con2[idx2].getX())
		{
			idx1++;
		}
		while (con2[idx2 + 1].getX() < con1[idx1].getX())
		{
			idx2++;
		}

		if (ifBelow(con1[idx1], con1[idx1 + 1], con2[idx2], con2[idx2 + 1]))
		{
			return con2;
		}
		else
		{
			return con1;
		}
	}

	while (con1[idx1 + 1].getX() < con2[idx2].getX())
	{
		idx1++;
	}
	while (con2[idx2 + 1].getX() < con1[idx1].getX())
	{
		idx2++;
	}

	if (ifBelow(con1[idx1], con1[idx1 + 1], con2[idx2], con2[idx2 + 1]))
	{
		con1isAbove = false;
	}
	else
	{
		con1isAbove = true;
	}

	idx1 = 0;
	idx2 = 0;//after the initial test of which one is above, put the indexes back to zero
	
	while (intersectIdx < intersectionPoints.size())
	{
		if (con1isAbove && intersectIdx < intersectionPoints.size())
		{
			while (con1[idx1].getX() != intersectionPoints[intersectIdx].getX() && con1[idx1].getY() != intersectionPoints[intersectIdx].getY())
			{
				newContour.push_back(con1[idx1]);
				idx1++;
			}
			while (con2[idx2].getX() != intersectionPoints[intersectIdx].getX() && con2[idx2].getY() != intersectionPoints[intersectIdx].getY())
			{
				idx2++;
			}
			newContour.push_back(con1[idx1]);
			idx1++;
			idx2++;
			intersectIdx++;
			con1isAbove = false;
		}

		if (!con1isAbove && intersectIdx < intersectionPoints.size())
		{
			while (con1[idx1].getX() != intersectionPoints[intersectIdx].getX() && con1[idx1].getY() != intersectionPoints[intersectIdx].getY())
			{
				idx1++;
			}
			while (con2[idx2].getX() != intersectionPoints[intersectIdx].getX() && con2[idx2].getY() != intersectionPoints[intersectIdx].getY())
			{
				newContour.push_back(con2[idx2]);
				idx2++;
			}
			newContour.push_back(con2[idx2]);
			idx1++;
			idx2++;
			intersectIdx++;
			con1isAbove = true;
		}
	}

	if (con1isAbove)
	{
		while (idx1 < con1.size())
		{
			newContour.push_back(con1[idx1]);
			idx1++;
		}
	}
	if (!con1isAbove)
	{
		while (idx2 < con2.size())
		{
			newContour.push_back(con2[idx2]);
			idx2++;
		}
	}



	return newContour;
	*/
	/*
	if (con1[0].getX() < con2[0].getX())
	{
		newContour.push_back(con1[0]);
	}
	if (con1[0].getX() > con2[0].getX())
	{
		newContour.push_back(con2[0]);
	}
	if (con1[0].getX() == con2[0].getX())
	{
		if (con1[0].getY() > con2[0].getY() || con1[0].getY() == con2[0].getY())
		{
			newContour.push_back(con1[0]);
		}
		else
		{
			newContour.push_back(con2[0]);
		}
	}
	*/

	//sample walk code
	/*
	int idx1 = 0;
	int idx2 = 0;

	while (con1[idx1 + 1].getX() < con2[idx2].getX())
	{
		//call a function that processes points (idx1, idx2)
		idx1++;
	}
	while (con2[idx2 + 1].getX() < con1[idx1].getX())
	{
		//call a function that processes points
		idx2++;
	}

	while (idx1 < con1.size() && idx2 < con2.size())
	{
		if (con1[idx1].getX() < con2[idx2].getX())
		{
			//function call
			idx1++;
		}
		else
		{
			//function call
			idx2++;
		}
	}

	while (idx1 < con1.size())
	{
		//function call
		idx1++;
	}

	while (idx2 < con2.size())
	{
		//function call
		idx2++;
	}
	*/
}

double Contours::yVal(double x, int conIdx)
{
	int i = 0;
	while (_contours[conIdx][i].getX() < x)
	{
		i++;
		if (i == _contours[conIdx].size() - 1)
		{
			break;
		}
	}

	if (i < _contours[conIdx].size() - 1 && i > 0)
	{
		double m = (_contours[conIdx][i + 1].getY() - _contours[conIdx][i].getY()) / (_contours[conIdx][i + 1].getX() - _contours[conIdx][i].getX());
		return m*x - m*_contours[conIdx][i].getX() + _contours[conIdx][i].getY();
	}
	else
	{
		return 0;
	}

	/*else if (i == 0)
	{
		return _contours[conIdx][0].getY();
		return 0;
	}
	else
	{
		int j = _contours[conIdx].size() - 1;
		return _contours[conIdx][j].getY();
	}*/

	/*
	{
		int j = _contours[conIdx].size() - 1;
		double m = (_contours[conIdx][j].getY() - _contours[conIdx][j - 1].getY()) / (_contours[conIdx][j].getX() - _contours[conIdx][j - 1].getX());
		return m*x - m*_contours[conIdx][j].getX() + _contours[conIdx][j].getY();
	}
	*/
}

std::vector<Vec3d> Contours::findHillPoints(double step)
{
	vector<Vec3d> interestingPoints;
	bool goingUp = true;
	double interestingHeight = -100;

	for (double i = -2.5; i < 2.5; i += step)
	{
		for (int j = 1; j < _contours.size(); j++)
		{
			if (j == 1)
			{
				if (yVal(i, 1) < yVal(i, 0))
				{
					interestingPoints.push_back(Vec3d(i, yVal(i, 0)));
					interestingHeight = yVal(i, 0);
					goingUp = false;
				}
			}
			if (goingUp && yVal(i, j) < yVal(i, j - 1))
			{
				if (yVal(i, j) > interestingHeight)
				{
					interestingPoints.push_back(Vec3d(i, yVal(i, j)));
					interestingHeight = yVal(i, j);
				}
				goingUp = false;
			}
			if (!goingUp && yVal(i, j) > yVal(i,j-1))
			{
				goingUp = true;
			}
			//if (j == _contours.size() - 1 && goingUp == true && yVal(i, j) > interestingHeight)
			//{
				//interestingPoints.push_back(Vec3d(i, yVal(i, j)));
			//}
			//cout << i << "        " << j << "         " << yVal(i, j) << endl;
		}
		interestingHeight = -100;
	}


	return interestingPoints;
}

double distanceBetweenVectors(Vec3d vec1, Vec3d vec2)
{
	return sqrt((vec1.getX() - vec2.getX())*(vec1.getX() - vec2.getX()) + (vec1.getY() - vec2.getY())*(vec1.getY() - vec2.getY()));
}

vector<vector<Vec3d>> Contours::findConnectedHillPoints(double step)
{
	bool goingUp = true;
	double interestingHeight = -100;
	vector<vector<Vec3d> > activeCurves;
	//vector<vector<Vec3d>> finishedCurves;

	
	for (double i = -2.5; i < 2.5; i += step)
	{
		for (int j = 1; j < _contours.size(); j++)
		{
			if (j == 1)
			{
				if (yVal(i, 1) < yVal(i, 0))
				{
					if (activeCurves.size() == 0)
					{
						vector<Vec3d> firstCurve;
						firstCurve.push_back(Vec3d(i, yVal(i, 0)));
						activeCurves.push_back(firstCurve);
					}
					else
					{
						bool activeCurveFound = false;
						for (int a = 0; a < activeCurves.size(); a++)
						{
							if (distanceBetweenVectors(Vec3d(i, yVal(i, j)), activeCurves[a][activeCurves[a].size() - 1]) < .05)
							{
								activeCurves[a].push_back(Vec3d(i, yVal(i, 0)));
								activeCurveFound = true;
							}
						}
						if (!activeCurveFound)
						{
							vector<Vec3d> newCurve;
							newCurve.push_back(Vec3d(i, yVal(i, 0)));
							activeCurves.push_back(newCurve);
						}
					}
					interestingHeight = yVal(i, j);
					goingUp = false;
				}
			}
			double first = yVal(i, j);
			double second = yVal(i, j - 1);

			if (goingUp && yVal(i, j) < yVal(i, j - 1))
			{
				if (yVal(i, j) > interestingHeight)
				{
					if (activeCurves.size() == 0)
					{
						vector<Vec3d> firstCurve;
						firstCurve.push_back(Vec3d(i, yVal(i, j)));
						activeCurves.push_back(firstCurve);
					}
					else
					{
						bool activeCurveFound = false;
						for (int a = 0; a < activeCurves.size(); a++)
						{
							if (distanceBetweenVectors(Vec3d(i, yVal(i, j)), activeCurves[a][activeCurves[a].size() - 1]) < .05)
							{
								activeCurves[a].push_back(Vec3d(i, yVal(i, j)));
								activeCurveFound = true;
							}
						}
						if (!activeCurveFound)
						{
							vector<Vec3d> newCurve;
							newCurve.push_back(Vec3d(i, yVal(i, j)));
							activeCurves.push_back(newCurve);
						}
					}
					interestingHeight = yVal(i, j);
				}
				goingUp = false;
			}
			if (!goingUp && yVal(i, j) > yVal(i, j - 1))
			{
				goingUp = true;
			}
		}
		interestingHeight = -100;
	}

	return activeCurves;
}

std::vector<Vec3d> Contours::findHorizon()
{
	vector<Vec3d> masterContour = _contours[0];

	for (int i = 1; i < _contours.size(); i++)
	//for (int i = 1; i < 4; i++)
	{
		masterContour = compareContours(masterContour, _contours[i]);
	}

	return masterContour;
}

void Contours::drawXOrder(int idx1, int idx2)
{
	int i = 0;
	int j = 0;


	glColor3f(1.0, 0.0, 0.0);

	while (i < _contours[idx1].size() && j < _contours[idx2].size())
	{
		if (_contours[idx1][i].getX() < _contours[idx2][j].getX())
		{
			glBegin(GL_LINES);
			{
				glVertex2d(_contours[idx2][j].getX(), _contours[idx2][j].getY());
				glVertex2d(_contours[idx1][i].getX(), _contours[idx1][i].getY());
			}
			glEnd();
			i++;
		}
		else
		{
			glBegin(GL_LINES);
			{
				glVertex2d(_contours[idx1][i].getX(), _contours[idx1][i].getY());
				glVertex2d(_contours[idx2][j].getX(), _contours[idx2][j].getY());
			}
			glEnd();
			j++;
		}
	}

	while (i < _contours[idx1].size())
	{
		glBegin(GL_LINES);
		{
			glVertex2d(_contours[idx1][i - 1].getX(), _contours[idx1][i - 1].getY());
			glVertex2d(_contours[idx1][i].getX(), _contours[idx1][i].getY());
		}
		glEnd();
		i++;
	}
	while (j < _contours[idx2].size())
	{
		glBegin(GL_LINES);
		{
			glVertex2d(_contours[idx2][j - 1].getX(), _contours[idx2][j - 1].getY());
			glVertex2d(_contours[idx2][j].getX(), _contours[idx2][j].getY());
		}
		glEnd();
		j++;
	}

	glColor3f(0.0, 0.0, 0.0);
}

