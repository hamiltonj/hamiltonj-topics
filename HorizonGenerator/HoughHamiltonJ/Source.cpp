/* fractals.c */

#include <iostream>
#include <GL/freeglut.h>
#include <cstdlib>
#include <string>
#include <cmath>
#include <ctime>
#include <vector>
#include "ImageDraw.h"
#include "Pic.h"
#include "Vec3d.h"
#include "Matrix.h"
#include "Segment.h"
#include "Grid.h"
#include "Contours.h"
#include "Dataset.h"

using namespace std;
#define FGH_PI       3.14159265358979323846

/* Flag telling us to keep executing the main loop */
static int continue_in_main_loop = 1;

/* the window title */
char window_title[80];

/* The amount the view is translated and scaled */
double xwin = 0.0, ywin = 0.0;
double scale_factor = 1.0;

// random value from -5 to 5  (double)
double randValue()
{
	return ((rand() % 10000) - 5000) / 1000.0;
}

Matrix generateXRotationMatrix(double theta);
Matrix generateYRotationMatrix(double theta);
Matrix generateZRotationMatrix(double theta);
Matrix generateTranslationMatrix(double a, double b, double c);
Matrix generateScaleMatrix(double sX, double sY, double sZ);
Matrix generateProjectionMatrix(double r, double l, double t, double b, double n, double f);


bool imageLoaded = false;
Pic *firstPic = nullptr;
Pic *secondPic = nullptr;
Pic *myGreyPic = nullptr;
Pic *myBlackWhitePic = nullptr;
Pic mySobelXPic(1, 1);
Pic mySobelYPic(1, 1);
Pic houghPic(1,1);
int counter = 0;
vector<double> kernel{ -1.0 / 8, -2.0 / 8, -1.0 / 8, 0, 0, 0, 1.0 / 8, 2.0 / 8, 1.0 / 8 };
Kernel sobely = Kernel(3, 3, kernel, false, 127.5);

Kernel createSobelX()
{
	std::vector<double> gX{ -1.0 / 8, 0, 1.0 / 8, -2.0 / 8, 0, 2.0 / 8, -1.0 / 8, 0, 1.0 / 8 };

	return Kernel(3, 3, gX, false, 128);
}

Kernel createSobelY()
{
	std::vector<double> gY{ -1.0 / 8, -2.0 / 8, -1.0 / 8, 0, 0, 0, 1.0 / 8, 2.0 / 8, 1.0 / 8 };

	return Kernel(3, 3, gY, false, 128);
}



vector<Vec3d> filteredProjectionPoints(double n, vector<Vec3d> &points) //clips points behind the near plane
{
	vector<Vec3d> newPoints;

	for (int i = 0; i < points.size(); i++)
	{
		
		if (points[i].getZ() < n)
		{
			newPoints.push_back(points[i]);
		}
	}

	return newPoints;
}

vector<Segment> filteredProjectionSegments(double n, vector<Segment> &segments) //clips points behind the near plane
{
	vector<Segment> newSegments;

	for (int i = 0; i < segments.size(); i++)
	{
		if (segments[i].v1Z() < n && segments[i].v2Z() < n)
		{
			newSegments.push_back(segments[i]);
		}
	}

	return newSegments;
}

vector<Vec3d> applyMatrix(const vector<Vec3d> &points, Matrix mat)
{
	vector<Vec3d> newVectors;
	for (int i = 0; i < points.size(); i++)
	{
		newVectors.push_back(mat.multiply(points[i]));
	}

	return newVectors;
}

vector<Segment> applyMatrix(const vector<Segment> &segments, Matrix mat)
{
	vector<Segment> newSegments;
	for (int i = 0; i < segments.size(); i++)
	{
		newSegments.push_back(mat.multiply(segments[i]));
	}

	return newSegments;
}

vector<Segment> generateSegments(const vector<Vec3d> &points, int row, int col)
{
	vector<Segment> newSegments;
	for (int i = 1; i < col; i++)
	{
		for (int j = 1; j < row; j++)
		{
			newSegments.push_back(Segment(points[i + j*col], points[i + j*col - 1]));
			newSegments.push_back(Segment(points[i + j*col], points[i + (j - 1)*col]));
		}
	}

	for (int i = 0; i < col - 1; i++)
	{
		newSegments.push_back(Segment(points[i], points[i + 1]));
	}
	for (int i = 0; i < row - 1; i++)
	{
		newSegments.push_back(Segment(points[i*col], points[(i + 1)*col]));
	}
	return newSegments;
}

void applyMatrixInPlace(vector<Vec3d> &points, Matrix mat)
{
	for (int i = 0; i < points.size(); i++)
	{
		points[i] = mat.multiply(points[i]);
	}
}

void applyMatrixInPlace(Grid &points, Matrix mat)
{
	for (int i = 0; i < points.width(); i++)
	{
		for (int j = 0; j < points.height(); i++)
		{
			points.at(i, j) = mat.multiply(points.at(i,j));
		}
	}
}

void drawPoints2D(vector<Vec3d> &points)
{
	glBegin(GL_POINTS);
	{
		for (int i = 0; i < points.size(); i++)
		{
			glVertex2d(points[i].getX(), points[i].getY());
		}
	}
	glEnd();
}

void drawPointsWithClipping(vector<Vec3d> &points, double n)
{
	glBegin(GL_POINTS);
	{
		for (int i = 0; i < points.size(); i++)
		{
			if (points[i].getZ() < n)
			{
				glVertex2d(points[i].getX(), points[i].getY());
			}
		}
	}
	glEnd();
}

void drawSegments2D(vector<Vec3d> &points, int row, int col)
{
	glBegin(GL_LINES);
	{
		for (int i = 1; i < col; i++)
		{
			for (int j = 1; j < row; j++)
			{
				glVertex2d(points[i + j*col].getX(), points[i + j*col].getY());
				glVertex2d(points[i + j*col - 1].getX(), points[i + j*col - 1].getY());
				glVertex2d(points[i + j*col].getX(), points[i + j*col].getY());
				glVertex2d(points[i + (j - 1)*col].getX(), points[i + (j - 1)*col].getY());
			}
		}

		for (int i = 0; i < col - 1; i++)
		{
			glVertex2d(points[i].getX(), points[i].getY());
			glVertex2d(points[i + 1].getX(), points[i + 1].getY());
		}
		for (int i = 0; i < row - 1; i++)
		{
			glVertex2d(points[i*col].getX(), points[i*col].getY());
			glVertex2d(points[(i + 1) * col].getX(), points[(i + 1) * col].getY());
		}
	}
	glEnd();
}

void drawSegments2D(vector<Segment> &segments)
{
	for (int i = 0; i < segments.size(); i++)
	{
		segments[i].draw();
	}
}

void quickSortX(vector<Vec3d> &points, int left, int right)
{
	int i = left;
	int j = right;
	Vec3d tmp;
	Vec3d pivot = points[(left + right) / 2];

	/*partition*/
	while (i <= j)
	{
		while (points[i].getX() < pivot.getX())
		{
			i++;
		}
		while (points[j].getX() > pivot.getX())
		{
			j--;
		}
		if (i <= j)
		{
			tmp = points[i];
			points[i] = points[j];
			points[j] = tmp;
			i++;
			j--;
		}
	}

	/*recursion*/
	if (left < j)
	{
		quickSortX(points, left, j);
	}
	if (i < right)
	{
		quickSortX(points, i, right);
	}


}

double vectorAngle(Vec3d vec1, Vec3d vec2, Vec3d vec3) // first vector vertices vec1 and vec2, second vector vertices vec1 and vec3
{
	Vec3d vecA(vec2.getX() - vec1.getX(), vec2.getY() - vec1.getY());
	Vec3d vecB(vec3.getX() - vec1.getX(), vec3.getY() - vec1.getY());

	double dotproduct = (vecA.getX() * vecB.getX()) + (vecA.getY() * vecB.getY());
	return acos(dotproduct / (vecA.magnitude() * vecB.magnitude()))* 180.0 / FGH_PI;
}

vector<Vec3d> findHorizon(vector<Vec3d> points, double xthresh) // points must be sorted by x
{
	vector<Vec3d> horizon;
	
	int previousIndex = 0;
	int currentIndex = 1;
	int nextIndex = 0;

	horizon.push_back(points[0]);
	while (currentIndex < points.size())
	{
		double bestAngle = 100000; //resets each test
		while (currentIndex < points.size() && points[currentIndex].getX() < points[previousIndex].getX() + xthresh)
		{
			double currentAngle = vectorAngle(points[previousIndex], Vec3d(points[previousIndex].getX(), points[previousIndex].getY() + 1, 0), points[currentIndex]);
			if (currentAngle < bestAngle)
			{
				bestAngle = currentAngle;
				nextIndex = currentIndex;
			}

			currentIndex++;
		}
		
		if (nextIndex == previousIndex)
		{
			nextIndex += 1;
			currentIndex += 1;
		}

		horizon.push_back(points[nextIndex]);
		previousIndex = nextIndex;
		currentIndex = nextIndex + 1;
	}

	horizon.push_back(points[points.size() - 1]);

	return horizon;
	
}

//Matrix projectYay = generateProjectionMatrix(50, -50, 50, -50, 50, -50);
Matrix projectYay = generateProjectionMatrix(2000, -2000, 2000, -2000, 2000, -2000);
Matrix translateYay = generateTranslationMatrix(0, 0,-50);
Matrix translateYayBig = generateTranslationMatrix(0, 0, -200);
Matrix translateYayReallyBig = generateTranslationMatrix(0, 0, -700);
Matrix rotateYay = generateYRotationMatrix(0);//FGH_PI /200);
Matrix rotateStandardAxis = generateXRotationMatrix(- FGH_PI / 2);
Grid grid1(61, 61);
Grid grid2(61, 61);
Grid grid3(61, 61);
Grid grid4(85, 85);
Grid grid5(256, 256);
//Grid grid6(976, 878);
Grid grid6(244, 219);
int contourCounter = 0;
vector<Vec3d> con1{ Vec3d(-20, 5), Vec3d(-10, 0), Vec3d(-5, -5), Vec3d(5, 10), Vec3d(15, -5), Vec3d(17.5, -2.5) };
vector<Vec3d> con2{ Vec3d(-25, -5), Vec3d(-22.5, -5), Vec3d(-20, -5), Vec3d(-15, -5), Vec3d(-7.5, 10), Vec3d(-2.5, 10), Vec3d(0, 0), Vec3d(10, 0), Vec3d(20, 10) };

vector<Vec3d> latticePoints;
bool displayBlack;
bool displayFundamentalHorizon;
bool displaySecondaryHorizon;
bool displayConnectedSecondaryHorizon;
int lastX;
int lastY;
int currentX;
int currentY;
bool clickDown;
double stepVal;

Dataset *dataset = NULL;

//create a class vertex that contains Vec3d, array of 8 pointers to points around it, always have the pointers going clockwise
//edge class that has pointers to two vertices and its two adjacent faces
//face class that has pointers to three vertices and pointers to three edges

static void
Display(void)
{	
	glClear(GL_COLOR_BUFFER_BIT);

	/* the curve */
	glPushMatrix();
	//glScalef(2.5, 2.5, 2.5);

	glColor4f(0.0, 0.0, 0.0, 1.0);

	glPointSize(2);

	if (displayBlack)
	{
		dataset->drawFullDatasetWithoutUpdating(1);
		//newGrid.drawWithClipping(1);
	}
	if (displayFundamentalHorizon)
	{
		dataset->drawHorizon(stepVal, 1);
	}
	if (displaySecondaryHorizon)
	{
		dataset->drawInterestingPoints(.01);
	}

	if (displayConnectedSecondaryHorizon)
	{
		dataset->drawConnectedInterestingPoints(.01);
	}

	glColor3f(0.0, 0.0, 0.0);

	
	glPopMatrix();
	glutSwapBuffers();
	//Display();
	glutPostRedisplay();
}

static void
Reshape(int width, int height)
{
	float ar;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1.0, 1.0, -1, 1, 2.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	xwin = 0.0;
	ywin = 0.0;
	glTranslated(xwin, ywin, -5.0);
}

static void
Key(unsigned char key, int x, int y)
{
	int need_redisplay = 1;

	switch (key) {
	case 27:  /* Escape key */
		continue_in_main_loop = 0;
		break;

	case '+':

		break;

	case '-':

		break;

	case 'r':
		/*
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		xwin = -1.0;
		ywin = 0.0;
		glTranslated(xwin, ywin, -5.0);
		*/
		//dataset->setGrid(grid);
		break;
	case 'R':
		//dataset->setGrid(grid);
		//dataset->updateAngle(0);
		break;
	case 'b':
		displayBlack = !displayBlack;
		break;
	case 'f':
		displayFundamentalHorizon = !displayFundamentalHorizon;
		break;
	case 's':
		displaySecondaryHorizon = !displaySecondaryHorizon;
		break;
	case 'c':
		displayConnectedSecondaryHorizon = !displayConnectedSecondaryHorizon;
		break;
	case 'i':
		dataset->applyMatrixInPlace(generateTranslationMatrix(0, 0, -1));
		dataset->updateProjectionGrid();
		break;
	case 'k':
		dataset->applyMatrixInPlace(generateTranslationMatrix(0, -1, 0));
		dataset->updateProjectionGrid();
		break;
	case '1':
		dataset = new Dataset(grid1, projectYay, translateYay, 0);
		dataset->updateProjectionGrid();
		stepVal = .1;
		break;
	case '2':
		dataset = new Dataset(grid2, projectYay, translateYay, 0);
		dataset->updateProjectionGrid();
		stepVal = .1;
		break;
	case '3':
		dataset = new Dataset(grid3, projectYay, translateYay, 0);
		dataset->updateProjectionGrid();
		stepVal = .1;
		break;
	case '4':
		dataset = new Dataset(grid4, projectYay, translateYayBig, 0);
		dataset->updateProjectionGrid();
		stepVal = .05;
		break;
	case '5':
		dataset = new Dataset(grid5, projectYay, translateYayBig, 0);
		dataset->updateProjectionGrid();
		stepVal = .01;
		break;
	case '6':
		dataset = new Dataset(grid6, projectYay, translateYayReallyBig, 0);
		dataset->updateProjectionGrid();
		stepVal = .02;
	default:
		need_redisplay = 0;
		break;
	}
	if (need_redisplay)
		glutPostRedisplay();
}

static void
Special(int key, int x, int y)
{
	int need_redisplay = 1;

	switch (key) {
	case GLUT_KEY_UP:
		glMatrixMode(GL_MODELVIEW);
		//ywin += 0.1 * scale_factor;
		//glTranslated(0.0, 0.1 * scale_factor, 0.0);

		for (int i = 0; i < con2.size(); i++)
		{
			con2[i].setY(con2[i].getY() + .01*scale_factor);
		}

		dataset->incrementAngleXAxis(-FGH_PI / 100);
		break;

	case GLUT_KEY_DOWN:
		glMatrixMode(GL_MODELVIEW);
		//ywin -= 0.1 * scale_factor;
		//glTranslated(0.0, -0.1 * scale_factor, 0.0);

		for (int i = 0; i < con2.size(); i++)
		{
			con2[i].setY(con2[i].getY() - .01*scale_factor);
		}

		dataset->incrementAngleXAxis(FGH_PI / 100);
		break;

	case GLUT_KEY_LEFT:
		glMatrixMode(GL_MODELVIEW);
		//xwin -= 0.1 * scale_factor;
		//glTranslated(-0.1 * scale_factor, 0.0, 0.0);
		for (int i = 0; i < con2.size(); i++)
		{
			con2[i].setX(con2[i].getX() - .01*scale_factor);
		}

		//grid.applyMatrixInPlace(generateYRotationMatrix(FGH_PI / 100));
		dataset->incrementAngle(FGH_PI / 100);

		break;

	case GLUT_KEY_RIGHT:
		glMatrixMode(GL_MODELVIEW);
		//xwin += 0.1 * scale_factor;
		//glTranslated(0.1 * scale_factor, 0.0, 0.0);

		for (int i = 0; i < con2.size(); i++)
		{
			con2[i].setX(con2[i].getX() + .01*scale_factor);
		}

		//grid.applyMatrixInPlace(generateYRotationMatrix(- FGH_PI / 100));
		dataset->incrementAngle(-FGH_PI / 100);

		break;

	case GLUT_KEY_PAGE_UP:
		glMatrixMode(GL_MODELVIEW);
		glTranslated(-xwin, -ywin, 0.0);
		glScaled(1.25, 1.25, 1.25);
		glTranslated(xwin, ywin, 0.0);
		scale_factor *= 0.8;
		break;

	case GLUT_KEY_PAGE_DOWN:
		glMatrixMode(GL_MODELVIEW);
		glTranslated(-xwin, -ywin, 0.0);
		glScaled(0.8, 0.8, 0.8);
		glTranslated(xwin, ywin, 0.0);
		scale_factor *= 1.25;
		break;

	default:
		need_redisplay = 0;
		break;
	}
	if (need_redisplay)
		glutPostRedisplay();
}


void MouseFunc(int button, int state, int x, int y)
{
	//std::cout << "Got a mouse event: " << button << " " << state << " " << x << " " << y << "\n";
}

void MouseMotion(int x, int y)
{
	//std::cout << "Motion: " << x << " " << y << std::endl;
	if (lastX == 0 || lastX == currentX)
	{
		lastX = x;
	}
	else
	{
		dataset->incrementAngle((x - lastX) * .005);
		lastX = x;
		currentX = lastX;
	}
	if (lastY == 0 || lastY == currentY)
	{
		lastY = y;
	}
	else
	{
		dataset->incrementAngleXAxis((y - lastY) * .005);
		lastY = y;
		currentY = lastY;
	}
}

void MousePassiveMotion(int x, int y)
{
	//std::cout << "Passive Motion: " << x << " " << y << std::endl;
}

int main(int argc, char *argv[])
{
	displayBlack = true;
	bool displayFundamentalHorizon = false;
	bool displaySecondaryHorizon = false;
	bool displayConnectedSecondaryHorizon = false;
	lastX = 0;
	lastY = 0;
	currentX = 0;
	currentY = 0;
	clickDown = false;
	stepVal = .1;


	//con1 = applyMatrix(con1, generateScaleMatrix(.05, .05, 1));
	//con2 = applyMatrix(con2, generateScaleMatrix(.05, .05, 1));
	//myPic = new Pic("C:\\Users\\Justin\\Documents\\C++\\hamiltonj-topics\\HorizonGenerator\\heightmap.jpg");
	firstPic = new Pic("C:\\heightmap.jpg");
	secondPic = new Pic("C:\\Users\\Justin\\Documents\\C++\\hamiltonj-topics\\HorizonGenerator\\Job260847_me2012_megis_midcoastal.png");
	
	
	for (double x = -30; x <= 30; x++)
	{
		for (double y = -30; y <= 30; y++)
		{
			grid1.at(x + 30, y + 30) = Vec3d(x, y, 10.0 * sin(x / 5.0) * cos(y / 5.0));
			grid2.at(x + 30, y + 30) = Vec3d(x, y, 10*sin(x*y/100.0));
			grid3.at(x + 30, y + 30) = Vec3d(x, y, 10 * sin(x * y/ 100.0) + y*.6);
			//grid.at(x + 30, y + 30) = Vec3d(x, y, 10.0* pow(x,-2) + 10.0* pow(y,-2));
			//grid.at(x + 30, y + 30) = Vec3d(x, y, tan(x*y)/10.0);
			//grid.at(x + 30, y + 30) = Vec3d(x, y, 20 - sqrt(x*x + y*y));
			

			//grid.at(x + 30, y + 30) = Vec3d(x, y, 10.0 * sin(x / 5.0) * cos(y / 5.0)*(10*cos(x / 10) / (1 + abs(x)))*(10*cos(y / 10) / (1 + abs(y))));

			//latticePoints.push_back(Vec3d(x, y, 10.0 * sin(x / 5.0) * cos(y / 5.0)));
			//latticePoints.push_back(Vec3d(x, y, x*x + 2*y*y));
			//latticePoints.push_back(Vec3d(x, y, 5 - sqrt(x*x + y*y)));
			//latticePoints.push_back(Vec3d(x, y, sqrt(9-x*x - y*y)));			
		}
	}

	for (double x = -127; x <= 128; x++)
	{
		for (double y = -127; y <= 128; y++)
		{
			grid5.at(x + 127, y + 127) = Vec3d(x, y, firstPic->pixel(x + 127, y + 127).g);
		}
	}


	int idxX = 0;
	int idxY = 0;
	for (double x = -127; x < 128; x+= 3)
	{
		for (double y = -127; y < 128; y+= 3)
		{
			grid4.at(idxX, idxY) = Vec3d(x, y, firstPic->pixel(x + 127, y + 127).g);
			idxY++;
		}
		idxY = 0;
		idxX++;
	}

	idxX = 0;
	idxY = 0;
	for (double x = -487; x <= 488; x += 5)
	{
		for (double y = -438; y <= 439; y += 5)
		{
			grid6.at(idxX, idxY) = Vec3d(x, y, secondPic->pixel(x + 487, y + 438).g);
			idxY++;
		}
		idxY = 0;
		idxX++;
	}
	

	dataset = new Dataset(grid1, projectYay, translateYay, 0);
	dataset->updateProjectionGrid();

	//grid.applyMatrixInPlace(rotateStandardAxis);

	srand(time(NULL));

	int fractal_window;

	glutInitWindowSize(500, 500);
	glutInitWindowPosition(140, 140);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInit(&argc, argv);
	fractal_window = glutCreateWindow(window_title);

	glClearColor(1.0, 1.0, 1.0, 1.0);

	glutMouseFunc(MouseFunc);
	glutMotionFunc(MouseMotion);
	glutPassiveMotionFunc(MousePassiveMotion);

	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Key);
	glutSpecialFunc(Special);
	glutDisplayFunc(Display);

#ifdef WIN32
#endif

	while (continue_in_main_loop)
		glutMainLoopEvent();

	printf("Back from the 'freeglut' main loop\n");

	return 0;             /* ANSI C requires main to return int. */
}
