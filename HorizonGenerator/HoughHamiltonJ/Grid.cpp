#include "Grid.h"
#include "Matrix.h"
#include "Vec3d.h"
#include <GL/freeglut.h>
#include <vector>

Grid::Grid()
{

}

Grid::Grid(int width, int height)
{
	_points.resize(width*height);
	_width = width;
	_height = height;
}

Grid::Grid(int width, int height, std::vector<Vec3d> points)
{
	_points = points;
	_width = width;
	_height = height;

}

void Grid::applyMatrixInPlace(Matrix mat)
{
	for (int i = 0; i < _width; i++)
	{
		for (int j = 0; j < _height; j++)
		{
			at(i, j) = mat.multiply(at(i, j));
		}
	}
}

Grid Grid::applyMatrix(Matrix mat)
{
	Grid newGrid(_width, _height);

	for (int i = 0; i < _width; i++)
	{
		for (int j = 0; j < _height; j++)
		{
			newGrid.at(i, j) = mat.multiply(at(i, j));
		}
	}

	return newGrid;
}

void Grid::drawWithClipping(double n)
{
	glBegin(GL_POINTS);
	{
		for (int i = 0; i < _width; i++)
		{
			for (int j = 0; j < _height; j++)
			{
				if (at(i, j).getZ() < n)
				{
					glVertex2d(at(i, j).getX(), at(i, j).getY());
				}
			}
		}
	}
	glEnd();
}


Grid::~Grid()
{
}
