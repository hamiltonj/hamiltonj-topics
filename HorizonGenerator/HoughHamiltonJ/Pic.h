#pragma once

#include <string>
#include "ImageDraw.h"
#include "Kernel.h"

class Pic
{
	std::string _filename;
	int _width;
	int _height;
	RGBPixel *_pixels;
	GLuint _textureId;

public:
	Pic(Pic &other, bool toGrey);
	Pic(const Pic &other);
	Pic(const char *filename);
	Pic(int width, int height);
	
   ~Pic();

   Pic &operator=(const Pic &other);

    void draw(double x, double y, double width, double height = 0);
	void save(const char *filename);

	int width() const { return _width;  }
	int height() const { return _height; }

	std::string filename() { return _filename;  }

	RGBPixel &pixel(int x, int y);

	int getGrey(int x, int y);
	void setGrey(int x, int y, int value);
	void derivativeX();
	void derivativeY();
	void derivativeXandY();


	void threshold(int level); // if the greyscale is bigger than level, set it to white, if its less, set it to black
	//if grey > level, make pixel white
	//else make them all black


	Pic apply(Kernel k);
	//Pic applySobelGradient();
	Pic houghTransform();
};



