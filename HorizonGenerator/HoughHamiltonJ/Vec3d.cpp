#include "Vec3d.h"
#include <iostream>

using namespace std;

Vec3d::Vec3d()
{
}

Vec3d::Vec3d(double x, double y, double z)
{
	_x = x;
	_y = y;
	_z = z;
}

Vec3d::~Vec3d()
{
}

void Vec3d::print()
{
	cout << "(" << getX() << "," << getY() << "," << getZ() << ")" << endl;
}

void Vec3d::setXYZ(double newXVal, double newYVal, double newZVal)
{
	_x = newXVal;
	_y = newYVal;
	_z = newZVal;
}

Vec3d &Vec3d::operator=(const Vec3d &other)
{
	_x = other.getX();
	_y = other.getY();
	_z = other.getZ();

	return *this;
}

double Vec3d::crossDir(Vec3d &other)
{
	return getX()*other.getY() - getY()*other.getX();
}